#!/bin/sh
echo
echo "Explanation:"
echo
echo "  All symbols with a:"
echo "  -T are global functions."
echo "  -t are static functions."
echo "  -D or"
echo "  -d are global or static data"
echo "     with initialization values in rom"
echo "  -B and"
echo "  -b are using RAM."
echo
echo "  The first row is the ADDRESS and the"
echo "  second is the size (both hex)."
echo
avr-nm --size-sort --print-size $1
