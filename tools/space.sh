#!/bin/sh
# Really quick and dirty solution for determining how many bytes of flash are
# free.

FLASH_SIZE=16384
BOOT_SIZE=512

FLASH_USED=$(avr-size $1|tail -1|awk '{print $1+$2}')
BSS_USED=$(avr-size $1|tail -1|awk '{print $3}')

printf "\nTotal flash:      %16d Bytes\n" $FLASH_SIZE
printf "Boot section:     %16d Bytes\n\n" $BOOT_SIZE
printf ".text+.data used: %16d Bytes\n" $FLASH_USED
printf ".bss used:        %16d Bytes\n\n" $BSS_USED

printf "%16d Bytes free\n\n" $(($FLASH_SIZE - $BOOT_SIZE - $FLASH_USED))
