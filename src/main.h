#ifndef _MAIN_H_
#define _MAIN_H_

        // main() does not return anything and it doesn't
        // need any prologue/epilogue sequences generated
        // by the compiler.
        // -----------------------------------------------
        // Additionally, we've added -ffreestanding to the
        // compiler flags.

        void main(void) __attribute__((noreturn, naked));

#endif // _MAIN_H_
