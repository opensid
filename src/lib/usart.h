#ifndef _LIB_USART_H_
#define _LIB_USART_H_

#include <avr/io.h>

extern void uartInit(void);
extern int uartPutc(const uint8_t);
extern int uartGetc(void);
extern int uartGetcWait(void);

#endif // _LIB_USART_H_
