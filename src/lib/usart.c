#include <avr/io.h>
#include <avr/interrupt.h>

#include "usart.h"
#include "fifo.h"

// The baudrate is set in 'src/hw/pinning.h'.
#include "../hw/pinning.h"

#define BUF_IN 0x40
uint8_t inbuf[BUF_IN];
fifo_t infifo;

#define BUF_OUT 0x40
uint8_t outbuf[BUF_OUT];
fifo_t outfifo;

void uartInit(void)
{
        uint8_t sreg = SREG;
        uint16_t ubrr = (uint16_t) ((uint32_t) F_CPU/(16*BAUDRATE) - 1);
        UBRRH = (uint8_t) (ubrr >> 8);
        UBRRL = (uint8_t) (ubrr);

        cli();

        UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
        UCSRC = (1 << URSEL) | (1 << UCSZ1 ) | (1 << UCSZ0);

        do
        {
                UDR;
        }
        while(UCSRA & (1 << RXC));

        UCSRA = (1 << RXC) | (1 << TXC);

        SREG = sreg;

        fifo_init(&infifo, inbuf, BUF_IN);
        fifo_init(&outfifo, outbuf, BUF_OUT);
}

int uartPutc(const uint8_t c)
{
        int ret = fifo_put(&outfifo, c);
        UCSRB |= (1 << UDRIE);
        return ret;
}

int uartGetc(void)
{
        return fifo_get_nowait(&infifo);
}

int uartGetcWait(void)
{
        return fifo_get_wait(&infifo);
}

SIGNAL(SIG_UART_RECV)
{
        _inline_fifo_put(&infifo, UDR);
}

SIGNAL(SIG_UART_DATA)
{
        if(outfifo.count > 0)
        {
                UDR = _inline_fifo_get(&outfifo);
        }
        else
        {
                UCSRB &= ~(1 << UDRIE);
        }
}
